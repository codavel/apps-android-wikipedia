### Codavel SDK ###

The purpose of this project is to demonstrate how easy is to integrate Codavel SDK into an Android app, by modifying apps-android-wikipedia, the official Wikipedia Android app.

You can check the original README [here](README.original.md), or directly in the original repository [here](https://github.com/wikimedia/apps-android-wikipedia).


### Integration ###

In order to integrate Codavel SDK, the following modifications to the original project were required (you can check all the code changes [here](https://gitlab.com/codavel/apps-android-wikipedia/-/commit/9872d4352a52b0d22614e3a1017d6e5740f08a3b)):

1. Added maven repository and Codavel's Application ID and Secret to the app's build.gradle, required to download and use Codavel's SDK
2. Start Codavel's Service when the main activity is created, so that our SDK can start processing the HTTP requests.
3. Register our HTTP interceptor into the app's OkHTTPConnectionFactory, so that all the HTTP requests executed through the app's OKHTTPClient are processed and forwarded to our SDK.
